

$(document).on('ready', function(){
    var socket = io.connect(window.location.host);

    socket.on('timer', function(seconds){
        $('#seconds').html(seconds.seconds)
    });
    socket.on('count', function(countdata){
        $('#redcount').html(countdata.red);
        $('#greencount').html(countdata.green);
        $('#bluecount').html(countdata.blue)
    })
    socket.on('previousColor', function(data){
        console.log(data)
        $('#previouscolor circle').attr("fill", data)
    })

    $('#red').on('click tap', function(){
        $.post("/vote", {color:'red'})
        .done(function(data){
                if(data.err){

                }else if(data.message){

                }
            })
    })
    $('#green').on('click tap', function(){
        $.post("/vote", {color:'green'})
            .done(function(data){
                if(data.err){

                }else if(data.message){

                }
            })
    })
    $('#blue').on('click tap', function(){
        $.post("/vote", {color:'blue'})
            .done(function(data){
                if(data.err){

                }else if(data.message){

                }
            })
    })
})