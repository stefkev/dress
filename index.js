var express = require("express");
var bluemix = require("bluemix")
var spark = require("spark")
var bodyParser = require("body-parser");
var session = require("express-session")
var MongoStore = require('connect-mongo')(session);
var uid = require("uid");
var jade = require("jade");
var util = require('util');


var redVoteCount = 0;
var greenVoteCount = 0;
var blueVoteCount = 0;


var store = new MongoStore({
    url: 'mongodb://justdoneit.pw/dress'
})

var prevColor;
var currentColor;

var username = 'luk@chunghay.com';
var password = 't$C9822s=237DKw';


spark.login({username: username, password: password}, function(err, body){
    if (err){
        console.log('error occured '+util.inspect(err));
    }else{
        console.log('Authentificated ' +util.inspect(body))
    }
})


var randomGenerator = function(min ,max){
    return Math.floor(Math.random()*(max-min+1)+min);
}

var updateColor = function(color){
    spark.callFunction('240039001447343339383037', 'color', color, function(err){
        if(err){
            console.log("error occured on calling function "+err)
        }
    })
}

var randomColor = function(){
    if (redVoteCount > greenVoteCount && redVoteCount > blueVoteCount){
        return 'red';
    }
    if (greenVoteCount > redVoteCount && greenVoteCount > blueVoteCount){
        return'green';
    }
    if (blueVoteCount > redVoteCount && blueVoteCount > greenVoteCount){
        return 'blue';
    }

    /*
     Random values if votes are same
     */
    if (redVoteCount == greenVoteCount && redVoteCount == blueVoteCount){
        var rand = randomGenerator(0, 2);

        if (rand == 0){
            return 'red'
        }
        if (rand == 1){
            return 'blue'
        }
        if(rand == 2){
            return 'green';
        }
    }else if (redVoteCount === greenVoteCount){
        var rand = randomGenerator(0, 1);
        if (rand === 0){
            return  'red';
        }
        if(rand === 1){
            return 'green'
        }
    }else if (redVoteCount === blueVoteCount){
        var rand = randomGenerator(0, 1);
        if (rand === 0){
            return 'red'
        }
        if (rand === 1){
            return 'blue'
        }
    }else if (blueVoteCount === greenVoteCount){
        var rand = randomGenerator(0, 1);
        console.log('rand nubmer is' + rand)
        if (rand === 0){
            return'blue'
        }
        if (rand === 1){
            return 'green'
        }
    }
}

setInterval(function(){
    if (seconds === 0){
        currentColor = randomColor();
        if (currentColor === 'red'){
            updateColor('reddish')
            io.emit('previousColor', 'rgb(245, 32, 177)')
        }
        if (currentColor === 'green'){
            updateColor('greenish')
            io.emit('previousColor', 'rgb(255, 199, 0)')
        }
        if (currentColor === 'blue'){
            updateColor('blueish')
            io.emit('previousColor', 'rgb(18, 198, 236)')
        }
        console.log('updated to '+currentColor)

        seconds = 20;
        redVoteCount = 0;
        blueVoteCount = 0;
        greenVoteCount = 0;
        store.clear();
        io.emit('count',{green: greenVoteCount, blue: blueVoteCount, red: redVoteCount})
    }
    io.emit('timer',{seconds:seconds});
    seconds--;
}, 1000)



var seconds = 20;



var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

io.on('connection', function(socket){
    io.emit('count',{green: greenVoteCount, blue: blueVoteCount, red: redVoteCount})
    if (currentColor === 'red'){
        io.emit('previousColor', 'rgb(245, 32, 177)')
    }
    if (currentColor === 'green'){
        io.emit('previousColor', 'rgb(255, 199, 0)')
    }
    if (currentColor === 'blue'){
        io.emit('previousColor', 'rgb(18, 198, 236)')
    }
})

//startup values
io.emit('previousColor', 'rgb(255, 199, 0)');
currentColor = 'green';
updateColor('greenish')
//=======================

app.use(express.static(__dirname + '/public'));
app.use('/bower_components',  express.static(__dirname + '/bower_components'));

app.set('view engine', 'jade')

app.use(bodyParser.urlencoded({ extended: false }))

var custSession = session({
    store: store,
    genid: function(req){
        return uid(10);
    },
    resave: false,
    saveUninitialized: true,
    secret : "leddress",
    locked: false,
})
app.use(bodyParser.json())

app.use(custSession);


app.get('/', function(req, res){
    res.render('index')
})

app.post('/vote', function(req, res){
    var color = req.body.color;
    if(req.session.locked != true){
        if(color === 'red'){
            redVoteCount++;
        }
        if(color === 'green'){
            greenVoteCount++;
        }
        if(color === 'blue'){
            blueVoteCount++
        }
        io.emit('count',{green: greenVoteCount, blue: blueVoteCount, red: redVoteCount})

        req.session.locked = true
        res.json({
            message: "You have voted for color "+color
        })
    }else{
        res.json({
            err: "Wait until voting time is expired, you have already voted"
        })
    }
})

var port = process.env.VCAP_APP_PORT || 3000;

server.listen(port);
